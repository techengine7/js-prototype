
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

function Student(name, point) {//конструктор студента
	this.name  = name;
	this.point = point;
}
Student.prototype.show = function () {
	console.log('Студент %s набрал %s баллов', this.name, this.point);
};

function StudentList(group, spArray) {//конструктор группы студентов
	this.group = group;
	var students = [];
	newStudentsArr = spArray || null;
	if (( newStudentsArr != null) && (Object.prototype.toString.call( newStudentsArr ) == '[object Array]') && (newStudentsArr.length != 0)){
	newStudentsArr.reduce( function(value, current, i) {
			if (i % 2 != 0){
				Array.prototype.push.call(students, new Student(value,current));
			}
			else {
				return current;
			}
	});
	}
	this.students = students;
	this.add  = function (name, point) {
		Array.prototype.push.call(this.students, new Student(name,point));
	}
}
//«Объект, созданный этим конструктором должен обладать всеми функциями массива»
StudentList.prototype = Object.create(Array.prototype);//наследование конструктором группы студентов методов из Array
StudentList.prototype.constructor = StudentList;

var hj2 = new StudentList('HJ-2', studentsAndPoints);
//Проверяем что Объект, созданный конструктором StudentList обладает всеми функциями массива
console.dir(typeof hj2.pop);
console.dir(typeof hj2.forEach);
console.dir(typeof hj2.reduce);

hj2.add('Татьяна Иванова',10);
hj2.add('Сергей Петров',20);
hj2.add('Юлия Алексеева',10);
hj2.add('Геннадий Дмитриев',20);
hj2.add('Дмитрий Егоров',20);

var html7 = new StudentList('HTML-7');
html7.add('Игорь Борисов', 0);
html7.add('Ирина Сидорова', 0);
html7.add('Кирилл Григорьев', 0);
html7.add('Леонид Сергеев', 0);
html7.add('Зинаида Жоричева',20);

StudentList.prototype.show = function () {
	console.log( "Группа %s (%s студентов):", this.group, this.students.length);
	this.students.forEach( function(student) { student.show(); });
};

	var idxRec; 
	var whoGoes = 'Татьяна Иванова';
	var studentOut = hj2.students.filter( function(thisStudent, i) {
		if (thisStudent.name == whoGoes) {
			idx = i;
			return (thisStudent);
		}
	});
	hj2.students.splice(idx, 1);
	html7.add(studentOut[0].name, studentOut[0].point);
	
hj2.show();
html7.show();

/*
Дополнительное задание
Добавить спискам студентов метод max, который возвращает студента с наибольшим баллом в этой группе. Изучите документацию метода valueOf в Object.prototype, и попробуйте использовать Math.max для решения этой задачи.
*/

Student.prototype.valueOf = function() { return this.point; }; //Переопределяем valueOf для объекта типа "Студент"

StudentList.prototype.max = function () {
	var maxN = this.students.map( function(theStudent) { return theStudent.valueOf(); });
	var myMax = Math.max.apply(null, maxN);
	console.log( 'Максимальный балл в группе '+this.group+' набрал студент: '+this.students[ maxN.indexOf(myMax) ].name );
};

hj2.max();
html7.max();